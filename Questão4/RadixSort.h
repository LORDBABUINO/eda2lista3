void RadixSort(int vetor[], int tamanho) {

    int i;
    int maior = vetor[0];
    for (i = 0; i < tamanho; i++) {
        if (vetor[i] > maior)
    	    maior = vetor[i];
    }
	
	int ordemDeGrandeza = 1;
	int ordenado[tamanho];
    while (maior/ordemDeGrandeza > 0) {
        int bucket[10] = { 0 };
    	for (i = 0; i < tamanho; i++)
    	    bucket[(vetor[i] / ordemDeGrandeza) % 10]++; 
    	for (i = 1; i < 10; i++)
    	    bucket[i] += bucket[i - 1];
    	for (i = tamanho - 1; i >= 0; i--)
    	    ordenado[--bucket[(vetor[i] / ordemDeGrandeza) % 10]] = vetor[i];
    	for (i = 0; i < tamanho; i++)
    	    vetor[i] = ordenado[i];
    	ordemDeGrandeza *= 10;
    }
}

